#%%global debug_package   %{nil}
%define specrelease 6%{?dist}
%if 0%{?openeuler}
%define specrelease 6
%endif

Name:           deepin-reader
Version:        5.9.9
Release:        %{specrelease}
Summary:        A simple PDF reader, supporting bookmarks, highlights and annotations
License:        GPLv3+
URL:            https://github.com/linuxdeepin/%{name}
Source0:        %{url}/archive/%{version}/%{name}-%{version}.tar.gz

BuildRequires: gcc-c++
BuildRequires: cmake
BuildRequires: qt5-devel

BuildRequires: dtkwidget-devel
BuildRequires: dtkcore-devel
BuildRequires: dtkgui-devel
BuildRequires: pkgconfig(ddjvuapi)
BuildRequires: pkgconfig(nss)
BuildRequires: pkgconfig(libjpeg)
BuildRequires: pkgconfig(cairo)
BuildRequires: openjpeg2-devel
BuildRequires: poppler-qt5-devel
BuildRequires: libspectre-devel
BuildRequires: kf5-karchive-devel
BuildRequires: libtiff-devel
BuildRequires: freetype-devel
BuildRequires: nspr-devel
BuildRequires: fontconfig-devel
BuildRequires: lcms2-devel
BuildRequires: libX11-devel
BuildRequires: qt5-qtbase-private-devel
BuildRequires: chrpath
BuildRequires: fdupes

%description
%{summary}.

%prep
%autosetup

%build
# help find (and prefer) qt5 utilities, e.g. qmake, lrelease
export PATH=%{_qt5_bindir}:$PATH
mkdir build && pushd build
%qmake_qt5 ../ DAPP_VERSION=%{version} DEFINES+="VERSION=%{version}"
%make_build
popd

%install
%make_install -C build INSTALL_ROOT="%buildroot"

pushd %{buildroot}/usr/lib/%{name}
mapfile -t dups <<<"$(fdupes -1 .)"
for line in "${dups[@]}"; do
    realfile="$(echo "${line}" | tr '[:space:]' '\n' | sort -r | head -1)"
    mapfile -t dupes <<<"$(echo "${line}" | tr '[:space:]' '\n' | sort -r | tail --lines=+2)"
    for dup in "${dupes[@]}"; do
        test -z "${dup}" && continue
        ln -s -f "${realfile}" "${dup}"
    done
done
popd

# remove rpath info
for file in $(find %{buildroot}/ -executable -type f -exec file {} ';' | grep "\<ELF\>" | awk -F ':' '{print $1}')
do
    if [ ! -u "$file" ]; then
        if [ -w "$file" ]; then
            chrpath -d $file
        fi
    fi
done

# add rpath path in ld.so.conf.d
mkdir -p %{buildroot}%{_sysconfdir}/ld.so.conf.d
echo "%{_bindir}/%{name}" > %{buildroot}/etc/ld.so.conf.d/%{name}-%{_arch}.conf
echo "/usr/lib/%{name}" >> %{buildroot}/etc/ld.so.conf.d/%{name}-%{_arch}.conf

%files
%doc README.md
%license LICENSE
%{_bindir}/%{name}
%{_datadir}/icons/hicolor/scalable/apps/%{name}.svg
%{_datadir}/%{name}/translations/*.qm
%{_datadir}/applications/%{name}.desktop
# /usr/lib/libdeepin-pdfium.*
%{_datadir}/deepin-manual/manual-assets/application/deepin-reader/document-viewer/*
/usr/lib/deepin-reader/libdeepin-poppler*
%config(noreplace) /etc/ld.so.conf.d/%{name}-%{_arch}.conf

%changelog
* Tue May 30 2023 leeffo <liweiganga@uniontech.com> - 5.9.9-6
- fix: fix install warning

* Tue Mar 07 2023 liweiganga <liweiganga@uniontech.com> - 5.9.9-5
- fix: enable debug

* Wed Dec 07 2022 liweiganga <liweiganga@uniontech.com> - 5.9.9-4
- fix: remove rpath

* Mon Dec 05 2022 liweiganga <liweiganga@uniontech.com> - 5.9.9-3
- fix: linkfile size and install warning

* Mon Dec 05 2022 liweiganga <liweiganga@uniontech.com> - 5.9.9-2
- fix: remove rpath

* Tue Jul 19 2022 konglidong <konglidong@uniontech.com> - 5.9.9-1
- update to 5.9.9

* Thu Feb 10 2022 liweigang <liweiganga@uniontech.com> - 5.7.0.21-2
- fix build error

* Wed Jul 07 2021 weidong <weidong@uniontech.com> - 5.7.0.21-1
- Update to 5.7.0.21

* Tue Sep 1 2020 chenbo pan <panchenbo@uniontech.com> - 5.6.9-2
- fix compile fail

* Thu Jul 30 2020 openEuler Buildteam <buildteam@openeuler.org> - 5.6.2-1
- Package init
